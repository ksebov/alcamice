import os.path
import cv2
import h5py
import numpy as np

def save_avi(folder, filename, data, fps, avi_format='DIVX'):
    fourcc = cv2.VideoWriter_fourcc(*avi_format)
    out = cv2.VideoWriter(os.path.join(folder, filename), fourcc, fps, (data.shape[1], data.shape[2]), isColor=False)

    frames = range(data.shape[0])
    for i in frames:
        frame = cv2.convertScaleAbs(data[i].astype('float'))
        out.write(frame)

    out.release()
    print("Wrote %i frames to %s" % ( len(frames), filename ))

class DataWriter:
    # Dataset parameters
    cx = 224
    label_size = 2 # fps, clss

    def __init__(self, folder, filename, erase=False
                     , isColor=False, dtype = 'uint8', compression = None
                     , save_avi=True
                ):
        self.folder = folder
        self.channels = 3 if isColor else 1

        self.avi_format = 'DIVX' if save_avi else None # TODO: None

        self.db_file = '%s-%i-%s' % (filename, self.channels, dtype)
        if compression:
            self.db_file += ('-' + compression)

        self.db_file += '.hdf5'

        self.db = None
        if not erase:
            try:
                self.db = h5py.File(os.path.join(folder, self.db_file),'r+')

                # Frames
                self.data   = self.db['data']
                self.index  = self.db['index']

                # Directory
                self.evt_start = self.db['evt_start']
                self.evt_label = self.db['evt_label']

            except OSError:
                print("%s no found. Creating..." % self.db_file)

        if not self.db:
            self.db = h5py.File(os.path.join(folder, self.db_file),'w')

            # Frames
            self.data   = self.db.create_dataset( 'data',   (0, self.cx, self.cx,self.channels), maxshape=(None, self.cx, self.cx, self.channels), dtype=dtype, compression=compression)
            self.index  = self.db.create_dataset( 'index',  (0,),                                maxshape=(None,),                                 dtype='int32') # Frame to Directory entry

            # Directory
            self.evt_start = self.db.create_dataset( 'evt_start', (0,),                 maxshape=(None,),                 dtype='int64') # First frame
            self.evt_label = self.db.create_dataset( 'evt_label', (0, self.label_size), maxshape=(None, self.label_size), dtype='int32')

        self.pos = 0

    def pre_process(self, frame):
        frame = cv2.resize(frame,(self.cx, self.cx))

        if self.channels == 1:
            frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

        return frame

    def save_frame(self, frame):
        if len(frame.shape)<3:
            frame = np.expand_dims(frame, -1)

        self.data.resize(self.pos+1, axis=0)
        assert self.pos < self.data.shape[0]

        self.data[self.pos] = frame
        self.pos += 1

        return self.pos

    def save_event(self, start, stop, clss, fps):
        label = [fps, clss]
        assert len(label) == self.label_size

        event_count = len(self.evt_start)
        assert event_count == self.evt_label.shape[0]

        new_event = event_count
        event_count += 1

        self.evt_start.resize((event_count,))
        self.evt_label.resize(event_count, axis=0)

        self.index.resize((stop,))

        self.evt_start[new_event] = start
        self.evt_label[new_event] = label

        self.index[start:stop] = new_event

        if self.avi_format:
            save_avi(self.folder, label, self.data[start:stop], fps)


