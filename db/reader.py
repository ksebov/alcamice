import h5py
import numpy as np
import os.path
from tqdm import tqdm

class DataReader:
    def __init__(self, filepath, num_classes):
        self.folder = os.path.dirname(filepath)
        self.db = h5py.File(filepath,'r')

        # Frames
        self.data   = self.db['data']
        self.index  = self.db['index']

        self.frame_count = len(self.index)
        assert self.frame_count <= self.data.shape[0]

        self.frame_shape = (self.data.shape[1], self.data.shape[2])

        # Directory--load entirely since HDF5 datasets don't support random indexing
        self.evt_start = np.array(self.db['evt_start'])
        self.evt_label = np.array(self.db['evt_label'])

        self.event_count = len(self.evt_start)
        assert self.event_count == self.evt_label.shape[0]

        self.sample_count =  len(self.index)
        assert self.sample_count <= len(self.data)

        #self.label_count = np.array([1161,    655,        12521,     1254,         2629,                  1338],                  dtype='float')
        self.label_count = np.zeros((num_classes,), dtype='float')
        for event in range(self.event_count):
            label = self.evt_label[event][1]

            start = self.evt_start[event]
            stop  = self.evt_start[event+1] if event < self.event_count-1 else self.sample_count

            self.label_count[label] += stop-start

        assert(self.label_count.sum()== self.sample_count)
        self.label_count /= self.label_count.sum()

        self.label_names =          ['empty', 'grooming', 'walking', 'looking up', 'rearing on the wall', 'rearing freely']
        assert num_classes == len(self.label_names)



    def dump_events(self, dump_avis=False, events=None, preview=None, out_csv=None):
        csv_writer = None if out_csv is None else csv.writer(open(os.path.join(self.folder, out_csv), 'w', newline=''))

        events = events if events else range(self.event_count)
        print( "Dumping %i events" % len(events))

        for i, event in enumerate(tqdm(events)):
            label = self.evt_label[event]

            start = self.evt_start[event]
            stop  = self.evt_start[event+1] if event < self.event_count-1 else self.data.shape[0]

            frames = range(start, stop)

            if dump_avis:
                save_avi(self.folder, start, stop, label, self.data[frames])

            if csv_writer:
                csv_writer.writerow([start, stop-1, label[1]])

            if preview:
                for frame in frames:
                    if not preview( self.data[frame] ):
                        break;
