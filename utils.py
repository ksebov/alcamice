import tensorflow as tf
import datetime as dt
import numpy as np

def get_optimizer(opt='adam'):
    if opt == "adam":
        optfn = tf.train.AdamOptimizer
    elif opt == "sgd":
        optfn = tf.train.GradientDescentOptimizer
    else:
        assert (False)
    return optfn

def get_session():
    """Create a session that dynamically allocates memory."""
    # See: https://www.tensorflow.org/tutorials/using_gpu#allowing_gpu_memory_growth
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)
    return session

class StopWatch():
    def __init__(self):
        self.then = dt.datetime.now()
        self.start = self.then

    def click(self, lap, from_start=False):
        now = dt.datetime.now()
        sec = (now-(self.start if from_start else self.then)).total_seconds()

        dur = ""
        if sec < 1:
            dur = " less than a second"
        else:
            min = sec // 60
            dur = " %i sec" % (sec % 60)
            if min > 0:
                hr = min // 60
                dur = " %i min" % (min % 60) + dur
                if hr > 0:
                    dur = " %i hr" % hr + dur

        print(lap + " in:"+dur)
        self.then = now

tf.reset_default_graph()
sess = get_session()

def get_uinitialized_vars(sess):
    uninitialized_vars = []
    for var in tf.global_variables():
        try:
            sess.run(var)
        except tf.errors.FailedPreconditionError:
            uninitialized_vars.append(var)

    return uninitialized_vars

def create_indices(data, shuffle=True):
    data_is_list = type(data) is list
    data_size = data[0].shape[0] if data_is_list else data.shape[0]
    indices = np.arange(data_size)

    if shuffle:
        np.random.shuffle(indices)

    return indices

def is_list(data):
    return type(data) is list

def partition(data, indices):
    indices = sorted(indices)
    return [d[indices,...] for d in data] if is_list(data) else data[indices,...]

def get_minibatches(data, minibatch_size, indices):
    data_is_list = is_list(data)

    for minibatch_start in np.arange(0, len(indices), minibatch_size):
        minibatch_indices = indices[minibatch_start:minibatch_start + minibatch_size]

        yield partition(data, minibatch_indices)

