from db.writer import *

import glob
import numpy as np
import cv2
import os.path
import h5py
import csv
from tqdm import tqdm

def crop(frame, x, y, w, h):
    return frame[y:y+h, x:x+w]

def deinterlace(frame):
    H,W,C = frame.shape
    assert(0 == H%2)
    fields = np.reshape(frame, (H//2, W*C*2))

    fields[:-1,W*C:]  = fields[:-1,:W*C]//2
    fields[:-1,W*C:] += fields[1: ,:W*C]//2

    return np.reshape(frame, (H, W, C))

def vframes(video):
    while(video.isOpened()):
        _,frame = video.read()
        if frame is None:
            break
        yield frame

def load_arena(filepath, arena, arena_name=None
             , preview=None
             , fps = 30, fourcc='MJPG'
             , fontScale=1, fontColor=(0, 0, 255)
             , dw=None
             , preprocess=None
              ):

    avi = None
    if fps:
        dot = filepath.rfind('.')
        out_name = filepath[:dot] if dot != -1 else filepath

        if arena_name:
            out_name += ('.'+ arena_name )

        out_name += '.avi'

        fourcc    = cv2.VideoWriter_fourcc(*fourcc)
        framesize = tuple(arena[2:])
        avi = cv2.VideoWriter(out_name, fourcc, fps, framesize, isColor=True)

    video = cv2.VideoCapture(filepath)

    for count, frame in enumerate(tqdm(vframes(video)
                                     , total=int(video.get(cv2.CAP_PROP_FRAME_COUNT))
                                     , unit='frames'
                                     , desc='Loading ' + os.path.basename(filepath)
                                      )):

        frame = crop(frame, *arena)

        if preprocess:
            frame = preprocess(frame)

        if dw:
            frame = dw.pre_process(frame)
            dw.save_frame(frame)

        if fontScale:
            cv2.putText(frame, str(count), (0, arena[3]), cv2.FONT_HERSHEY_PLAIN, fontScale, fontColor, 1)

        if avi:
            avi.write(frame)

        if preview and not preview(frame):
            break;

    if avi:
        avi.release()

def load_arenas(filename, arena_size, arenas_orig, arenas=None, **kwargs ):

    arenas = arenas if arenas else range(len(arenas_orig))

    def arena_name(i):
        return 'arena-'+str(i+1)

    for i in arenas:
        load_arena(filename, arenas_orig[i]+arena_size, arena_name=arena_name(i), **kwargs)


def load_class_folder(folder, crop_area=None, downsize=None, ext='png', preview=None):
    template = folder+'/*.'+ext
    filelist = sorted(glob.glob(template))
    for file in filelist:
        frame = cv2.imread(file)

        if crop_area:
            cropped = crop(frame, *crop_area)

        if downsize:
            cropped = cv2.resize(cropped, downsize, interpolation=cv2.INTER_AREA)

        done = preview and not preview(cropped)
        if done:
            break;

def load_folder(folder, **kwargs):
    class_names = sorted(os.listdir(folder))
    for class_name in class_names:
        class_folder = os.path.join(folder, class_name)
        if not os.path.isdir(class_folder):
            continue

        load_class_folder(class_folder, **kwargs)

def save_avi(folder, start, stop, label, data, avi_format='DIVX'):
    fps, clss = label
    filename = "%i-%i.avi" % (start, stop)

    filepath = os.path.join(folder, str(clss))
    if not os.path.exists(filepath):
        os.makedirs(filepath)

    filepath = os.path.join(filepath, filename)

    fourcc = cv2.VideoWriter_fourcc(*avi_format)
    out = cv2.VideoWriter(filepath, fourcc, fps, (data.shape[1], data.shape[2]), isColor=False)

    frames = range(data.shape[0])
    for i in frames:
        frame = cv2.convertScaleAbs(data[i].astype('float'))
        out.write(frame)

    out.release()

class CsvReader:
    pos = 0

    def load_events(self, in_csv, db
                  , default_clss
                  , fps=30
                  , min_default=10
                  , out_csv=None
                   ):
        reader = csv.reader( open(in_csv,  'r', newline=''))
        folder = os.path.dirname(in_csv)

        writer = None
        if out_csv :
            writer = csv.writer(open(os.path.join(folder, out_csv), 'w', newline=''))

        next(reader) # Skip the header

        #TODO Append
        self.pos = 0
        if db:
            db.index.resize(0, axis=0)
            db.evt_start.resize(0, axis=0)
            db.evt_label.resize(0, axis=0)

        def save_event(stop, clss):
            if db:
                db.save_event(self.pos, stop, clss, fps)

            new_row = [self.pos, stop-1, clss]
            if writer:
                writer.writerow(new_row)
            else:
                print(*new_row)

            self.pos = stop

        for row in reader:
            start, stop, clss, *_ = row

            start = int(start)
            stop  = int(stop)+1

            assert start >= self.pos
            if start - self.pos > min_default:
                save_event(start, default_clss)

            dot = clss.rfind('.')
            clss_id = int(clss[dot+1:])

            save_event(stop, clss_id)

        if db:
            last = db.data.shape[0]
            if last > self.pos:
                save_event(last, default_clss)

            assert db.data.shape[0] == self.pos

def display_preview(frame, resize= (256,256), time=None, window="frame"):
    display = frame#cv2.convertScaleAbs(frame.astype('float'))

    if resize:
        display = cv2.resize(display, resize)

    if time:
        cv2.putText(display, time.isoformat(' '), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255, 1)

    cv2.imshow(window, display)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()

        return False

    return True

if False:#__name__ == '__main__':
    start = dt.datetime.now()

    alcamice_folder = "E:/Dropbox/Stanford/cs231n/project/alkamice"
    """
    arena_file = "590-597.MPG"
    arena_size = [130,116]
    arenas_orig = [[50,  0,], [174,  0], [50,112,], [174,112], [406,  1], [530,  0], [406,115], [530,114]]
    """
    arena_file = "Group_1.MPG"
    arena_size = [130,116]
    arenas_orig = [[406,  0], [530,  0], [406,105], [530,105]]

    load_arenas(os.path.join(alcamice_folder, arena_file), arena_size, arenas_orig
              #, preprocess = lambda x: 1-x
              , fps=30, fourcc='MJPG', fontScale=1
              #, preview=lambda *args, **kwargs: display_preview( *args, resize=None, **kwargs)
               )
    """

    db_name = "open_field_vid1"
    arena = [230, 0, 500, 480]
    create = False

    dw = DataWriter("e:/tmp", db_name
                  , save_avi=False
                  #, dtype='float16'
                  #, compression='lzf'
                  #, isColor=true
                  , erase=create
                   )

    if create:
        load_arena( os.path.join(alcamice_folder, db_name+'.avi'), arena
                  #, preprocess = lambda x: 1-x
                  , dw=dw
                  , fps=0, fourcc='DIVX', fontScale=1
                  #, preview=lambda *args, **kwargs: display_preview( *args, resize=None, **kwargs)
                   )

    if True:
        CsvReader().load_events(os.path.join(alcamice_folder, db_name+'.csv')
              , default_clss=2 #walking
              , db=dw
              , out_csv='d:/tmp/test.csv'
               )

    DataReader("e:/tmp/open_field_vid1-1-uint8.hdf5").dump_events(
               dump_avis=True
              );


    crop_area=[230, 0, 500, 480]
    load_folder(os.path.join(alcamice_folder, db_name), crop_area=crop_area, preview=lambda *args, **kwargs: display_preview( *args, **kwargs))#, resize=None))
    """

    print("Total processing time:", dt.datetime.now()-start)

