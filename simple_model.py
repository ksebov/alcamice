from utils import *

import numpy as np
import tensorflow as tf

from cs231n.squeezenet import SqueezeNet
from cs231n.image_utils import SQUEEZENET_MEAN, SQUEEZENET_STD

tf.app.flags.DEFINE_string ("squeezenet_path", 'datasets/squeezenet.ckpt', "Path to init Squeezenet from")
tf.app.flags.DEFINE_string ("optimizer", "adam", "adam / sgd")
tf.app.flags.DEFINE_bool   ("finetune_cnn", True, "Fine tune CNN layers.")
tf.app.flags.DEFINE_integer("num_features", 256, "Number of event features.")
tf.app.flags.DEFINE_integer("num_classes", 6, "Number of event classes.")

FLAGS = tf.app.flags.FLAGS

class SimpleModel(SqueezeNet):
    def __init__(self, add_train=True):
        self.dropout = tf.placeholder_with_default(tf.constant(1, tf.float32), shape=[], name='dropout')

        with tf.variable_scope('preprocessing'):
            self.grayscale_input = tf.placeholder('uint8',shape=[None,None,None,1])
            inimage = tf.image.grayscale_to_rgb(self.grayscale_input)

            if add_train:
                with tf.variable_scope('augmentation'):
                    self.aug = tf.placeholder_with_default(tf.constant(0, tf.int32), shape=[])

                    inimage = tf.map_fn(lambda img: tf.image.rot90(img, self.aug%4), inimage)

                    inimage = tf.where((self.aug/4)%2 < 1, inimage, tf.map_fn(lambda img: tf.image.flip_up_down(img), inimage))

                    inimage = tf.where((self.aug/8)%2 < 1, inimage, tf.cast(255- tf.to_int32(inimage), 'uint8'))

            with tf.variable_scope('normalization'):
                normalized_image = (tf.to_float(inimage)/255.0 - SQUEEZENET_MEAN) / SQUEEZENET_STD

            tf.summary.image('normalized_image', normalized_image)

        super().__init__((self.grayscale_input, normalized_image), num_classes=0)
        self.sqeezenet_vars = tf.global_variables().copy()

        x = self.features

        if not FLAGS.finetune_cnn:
            x = tf.stop_gradient(x)

        num_features = FLAGS.num_features
        evt_features = None

        with tf.variable_scope('event_features') as vs:
            x = tf.layers.conv2d(x, num_features, 1, padding='valid', kernel_initializer=tf.contrib.layers.xavier_initializer(), use_bias=True, activation=tf.nn.relu, name='layer0')
            x = tf.layers.average_pooling2d(x, 13, strides=13, padding='valid', name='layer1')
            x = tf.layers.dropout(x, self.dropout)

            vs.reuse_variables()
            tf.summary.histogram('weights', tf.get_variable('layer0/kernel'))
            tf.summary.histogram('bias',    tf.get_variable('layer0/bias'))

            evt_features = tf.reshape(x,[-1, num_features])

        assert evt_features.get_shape().as_list() == [None, num_features]

        num_classes = FLAGS.num_classes #TODO

        with tf.variable_scope('simple_classifier'):
            x = evt_features
            x = tf.layers.dense(x, num_features, use_bias=True, activation=tf.nn.relu, name='layer0')
            x = tf.layers.dropout(x, self.dropout)
            x = tf.layers.dense(x, num_classes,  use_bias=True,                        name='layer1')

            self.logits = x

        assert self.logits.get_shape().as_list() == [None, num_classes]

        self.pred = tf.argmax(self.logits, 1, name='prediction')

        if add_train:
            with tf.variable_scope('loss'):
                self.label_count = tf.placeholder('float',shape=[num_classes])
                label_weights = 1/tf.gather(self.label_count, self.labels)
                self.loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.labels, logits=self.logits)*label_weights)

            with tf.variable_scope('train'):
                step = tf.Variable(0, trainable=False)
                rate = tf.train.exponential_decay(FLAGS.learning_rate, step, 1, FLAGS.learning_rate_decay)
                optimizer = get_optimizer(FLAGS.optimizer)(rate)
                self.train = optimizer.minimize(self.loss)

            with tf.variable_scope('evaluation'):
                with tf.variable_scope('accuracy'):
                    self.acc  = tf.reduce_mean(tf.to_float(tf.equal(self.pred, tf.to_int64( self.labels))))

                self.conf = tf.confusion_matrix(labels=self.labels, predictions=self.pred, num_classes=num_classes, weights=tf.to_int32(label_weights))
                confusion_image = tf.reshape( tf.cast(self.conf, tf.float32), [1, num_classes, num_classes, 1])

                with tf.variable_scope('F1'):
                    rearing_gt   = self.labels >= 4 ##TODO: Rearing classes
                    rearing_pred = self.pred  >= 4

                    precision_var, update_precision = tf.metrics.precision(labels=rearing_gt, predictions=rearing_pred)
                    recall_var,    update_recall    = tf.metrics.recall   (labels=rearing_gt, predictions=rearing_pred)

                    self.f1 = 2* precision_var * recall_var /(precision_var + recall_var + 1e-6)
                    self.update_eval = tf.group(update_precision, update_recall)

            tf.summary.scalar('simple_loss', self.loss)
            tf.summary.scalar('simple_acc',  self.acc)
            tf.summary.scalar('simple_f1',   self.f1)
            tf.summary.image('confusion',confusion_image)

    def load_squeezenet(self, sess, squeezenet_path):
        tf.train.Saver(self.sqeezenet_vars).restore(sess, squeezenet_path)

    def get_hps(self):
        return "lr%f-ld%f-tune%s" % (FLAGS.learning_rate, FLAGS.learning_rate_decay, "ON" if FLAGS.finetune_cnn else "OFF")

