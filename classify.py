import numpy as np
import os.path, time
import tensorflow as tf
import cv2

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

from db.reader import DataReader
from simple_model import SimpleModel
from utils import *

from parsedata import * #TODO vutils

FLAGS = tf.app.flags.FLAGS

if __name__ == '__main__':

    video_folder = "E:/Dropbox/Stanford/cs231n/project/alkamice"

    video_filename, cropv = 'Group_1.arena-3.avi', None # black on blue
    video_filename, cropv = 'open_field_vid1.avi', None
    video_filename, cropv = 'open_field_vid1-arena-1.avi', None
    #video_filename, cropv = 'rat-1.mp4', (75,30,290,300)
    #video_filename, cropv = 'sor_2obj.wmv', None
    #video_filename, cropv = 'vEJsUMS7GUQ.mp4',(60,15, 520, 470)
    #video_filename, cropv = 'kE-gd2enYik.mp4',(180,135, 520, 520)
    #video_filename, cropv = 'Focal_animal.avi',(140,5, 520, 520)
    #video_filename, cropv = '3wvSD7VzE1k.mp4', (95,90,210,220)

    model_folder = "cp"
    #model_name = '2017-06-09-22-51-35-3195'
    #model_name = '2017-06-08-23-07-23-3195'
    #model_name = '2017-06-08-08-40-46-3195'
    model_name = '2017-06-11-00-28-45-90-1595'
    model_name = '2017-06-11-07-26-43-90-1435'
    model_name = '2017-06-11-14-01-54-90-1475'

    stopwatch = StopWatch()

    sess = get_session()
    model = SimpleModel(add_train=False)
    model_softmax = tf.nn.softmax(model.logits, dim=1)

    batch_size = 1
    correct_scores = tf.gather_nd(model.logits, tf.stack((tf.range(batch_size, dtype=tf.int64), model.pred), axis=1))

    grads = tf.gradients(correct_scores, model.image)[0]
    assert grads.get_shape().as_list() == model.image.get_shape().as_list()
    model_saliency = tf.reduce_max(tf.abs(grads), axis=3)

    saver = tf.train.Saver()
    model_path = os.path.join('cp', model_name)

    saver.restore(sess, model_path)

    stopwatch.click('Model loaded')

    filepath = os.path.join(video_folder, video_filename)

    video = cv2.VideoCapture(filepath)
    preview = lambda frame: display_preview(frame, resize=(1024,512))

    def classify(frame):
        frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        frame = np.reshape(frame, (1,224, 224,1))

        sm, sal = sess.run([model_softmax, model_saliency], {model.grayscale_input:frame, model.dropout:0})

        return sm[0], sal[0]

    label_names = ['empty', 'grooming', 'walking', 'looking up', 'rearing on the wall', 'rearing freely']

    M = cv2.getRotationMatrix2D((112,112),270,1)
    cmap = plt.get_cmap('jet')

    for frame in vframes(video):
        if cropv:
            frame = crop(frame, *cropv)

        if False:
            image = cv2.GaussianBlur(frame, (0, 0), 3);
            image = cv2.addWeighted(frame, 1.5, image, -0.5, 0);

        frame = cv2.resize(frame,(224, 224))
        #frame = cv2.warpAffine(frame,M,(224,224))
        #frame = cv2.flip(frame,1)

        #frame = 255-frame
        sm, sal = classify(frame)

        order = np.argsort(sm)[::-1]

        annotated = frame.copy()
        fontColor= (0, 0, 255) if order[0] < 4 else (100,255,255)
        y = 20
        for c in order:
            if sm[c] < 0.01:
                break

            label = label_names[c] if sm[c] > .99 else "%2.0f%% %s" % (sm[c]*100, label_names[c])
            cv2.putText(annotated, label, (0, y), cv2.FONT_HERSHEY_PLAIN, 1, fontColor, 1)
            y += 20

        minsal = np.amin(sal)
        maxsal = np.amax(sal)
        sal[0] = np.arange(minsal, maxsal, (maxsal-minsal)/224, dtype=np.float)[:224]

        heatmap =  cmap(sal) #RGBA
        heatmap = np.delete(heatmap , 3, 2)
        heatmap = cv2.convertScaleAbs(heatmap, alpha=255)

        blend = cv2.addWeighted(heatmap, 0.5, frame, 0.5, 0);

        combined = np.hstack((annotated, blend))
        if not preview(combined):
            break



