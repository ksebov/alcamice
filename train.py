import numpy as np
import os.path, time
import tensorflow as tf

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

from db.reader import DataReader
from simple_model import SimpleModel
from utils import *

tf.app.flags.DEFINE_string ("continue_from", "", "Init the model from the checkpoint")
tf.app.flags.DEFINE_bool   ("verify_val", False, "Verify if validation dataset contains all classes")
tf.app.flags.DEFINE_string ("checkpoint", 'cp', "Path to load the model from")
tf.app.flags.DEFINE_integer("data_size", 0, "Limit total number of data samples, 0 to use whole set")
tf.app.flags.DEFINE_integer("epochs", 6, "Number of epochs to train.")
tf.app.flags.DEFINE_integer("augments", 16, "Number of epochs to augmentation transformations")
tf.app.flags.DEFINE_integer("batch_size", 64, "Batch size")
tf.app.flags.DEFINE_integer("val_size", 256, "Validation batch size")
tf.app.flags.DEFINE_float  ("learning_rate", 0.0001, "Learning rate.")
tf.app.flags.DEFINE_float  ("learning_rate_decay", 0.9999, "Learning rate.")
tf.app.flags.DEFINE_float  ("dropout", 0.5, "Dropout")

FLAGS = tf.app.flags.FLAGS

def get_model_name(when):
    return when.strftime('%Y-%m-%d-%H-%M-%S-') + model.get_hps() + "-batch%i-aug%i-do%f" % (FLAGS.batch_size, FLAGS.augments, FLAGS.dropout)

stopwatch = StopWatch()

model = SimpleModel()
step = -1

if FLAGS.continue_from:
    try:
        last_dash = FLAGS.continue_from.rfind('-')
        step = int(FLAGS.continue_from[last_dash+1:])
        model_name = FLAGS.continue_from[:last_dash]
    except ValueError:
        assert -1 == step

    saver = tf.train.Saver()
    model_path = os.path.join('cp', FLAGS.continue_from)

    saver.restore(sess, model_path)

    stopwatch.click("Model %s loaded" % model_name )
else:
    model.load_squeezenet(sess, FLAGS.squeezenet_path)
    model_name = get_model_name(stopwatch.start)

    uninitialized_vars = get_uinitialized_vars(sess)#TODO? tf.report_uninitialized_variables()
    sess.run( tf.variables_initializer(uninitialized_vars))

    stopwatch.click("Model %s initialized" % model_name )

tb_summaries = tf.summary.merge_all()

mice =    DataReader("datasets/open_field_vid1-1-uint8.hdf5", FLAGS.num_classes)

data_size = mice.sample_count

whole_data = [mice.data, mice.index]
epochs = FLAGS.epochs

if FLAGS.data_size > 0 :
    data_size = min(data_size , FLAGS.data_size)

stopwatch.click("Data loaded")

data_indices = create_indices(whole_data)[:data_size]

train_indices = data_indices[:data_size*8//10]
test_indices  = data_indices[ data_size*8//10: data_size*9//10]
val_indices   = data_indices[                  data_size*9//10:]

test = partition(whole_data, test_indices)
val  = partition(whole_data, val_indices)

def batch_Xy(batch):
    X, evt = batch
    y = mice.evt_label[evt][:,1]

    return (X, y)

if FLAGS.verify_val:
    val_X, val_y = batch_Xy(val)
    val_yu = np.unique(val_y)
    print(val_yu)
    assert ( val_yu == range(num_classes)).all()

stopwatch.click("Data patrtitioned")

tb_writer = tf.summary.FileWriter(os.path.join('tb', model_name), sess.graph )

saver = tf.train.Saver()
save_path = os.path.join('cp', model_name)

confusion = None
augmentations = FLAGS.augments-1
epoch_batches = 1+(len(train_indices)-1)//FLAGS.batch_size
for epoch in range(epochs):
    for bix, batch in enumerate(get_minibatches(whole_data, FLAGS.batch_size, train_indices)):
        step+=1
        def run_batch(Xy, aug, oplist, dropout_rate=FLAGS.dropout):
            return sess.run( oplist, {model.input:Xy[0], model.labels:Xy[1], model.label_count:mice.label_count, model.aug:aug, model.dropout:dropout_rate})

        for aug in range(augmentations):
            _, train_loss, train_acc = run_batch(batch_Xy(batch), aug, [model.train, model.loss, model.acc])
            print("Epoch %i/%i, batch: %i.%i/%i, Loss/acc/f1: %g / %.2f" %( epoch+1, epochs, bix, aug, epoch_batches, train_loss, 100*train_acc))

        if step % 5  == 0:  # Record summaries and test-set accuracy
            sess.run( tf.local_variables_initializer())#TODO

            for val_batch in get_minibatches(whole_data, FLAGS.val_size, val_indices):
                summary, val_loss, val_acc, confusion, _ = run_batch(batch_Xy(val_batch), augmentations, [tb_summaries, model.loss, model.acc, model.conf, model.update_eval], dropout_rate=0)

            #val_f1 = sess.run([model.f1])
            val_f1 = model.f1.eval(session=sess)

            tb_writer.add_summary(summary, step)
            print("val: %g / %.2f / %.2f" %( val_loss, 100*val_acc, 100*val_f1 ))

            saver.save(sess, save_path + ("-90" if val_f1 >= 0.9 else ""), step)

            stopwatch.click("Milestone completed")


#TODO stopwatch.click("Model trained")
stopwatch.click("Total processing completed", from_start=True)

plt.imshow(confusion, cmap=plt.cm.hot)

plt.savefig(model_name+'.png')

